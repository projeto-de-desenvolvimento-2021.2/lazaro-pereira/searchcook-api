const mongoose = require("mongoose");
const Schema = mongoose.Schema;
let RecipesSchema = new Schema({
  name: {
    type: String,
    required: true,
    max: 100,
  },
  category: {
    type: String,
    required: true,
  },
  ingredients: {
    type: Object,
    required: true,
  },
  quantities: {
    type: Object,
    required: false,
  },
  measures: {
    type: Object,
    required: false,
  },
  prepare: {
    type: String,
    required: true,
  },
  owner: {
    type: String,
    required: false,
  },
  photo: {
    type: String,
    required: true,
  },
});


module.exports = mongoose.model("Recipes", RecipesSchema);
