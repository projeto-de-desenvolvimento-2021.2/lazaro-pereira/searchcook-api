const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/users.model");
const authConfig = require("../config/auth");
const express = require("express");

function tokenGenerator(params = {}) {
  return jwt.sign({ params }, authConfig.secret, {
    expiresIn: 1500,
  });
}

exports.authenticate = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email }).select("+password");

  if (!user) {
    return res.status(400).send({ error: "User not found" });
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).send({ error: "Incorrect password" });
  }

  user.password = undefined;

  res.send({ user, token: tokenGenerator({ id: user.id }) });
};
