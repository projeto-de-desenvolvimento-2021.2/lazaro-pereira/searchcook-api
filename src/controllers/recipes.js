const Recipe = require('../models/recipes.model');

exports.createRecipe = async (req, res) => {
  try {
    const recipe = await Recipe.create(req.body);
    return res.send({ recipe });
  } catch (e) {
    return res.status(400).send({ error: 'Failed' });
  }
};

exports.getAllRecipes = async (req, res) => {
  Recipe.find(function (err, recipe) {
    if (err) return next(err);
    //res.send({ recipe });
    res.send({ response: recipe});
  });
};

exports.getRecipeById = async (req, res, next) => {
  Recipe.findById(req.params.id, function (err, recipe) {
    if (err) return next(err);
    res.send({response: recipe});
  });
};

exports.updateRecipe = async (req, res, next) => {
  Recipe.findByIdAndUpdate(req.params.id, {$set: req.body},function (err, recipe) {
    if (err) return next(err);
    res.send({response: recipe});
  });
};

exports.deleteRecipe = async (req, res, next) => {
  Recipe.findByIdAndRemove(req.params.id, function (err) {
    if (err) return next(err);
    res.send('Deleted!');
  });
};


