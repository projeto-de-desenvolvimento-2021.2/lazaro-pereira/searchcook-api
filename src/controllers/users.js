const User = require('../models/users.model');
const jwt = require("jsonwebtoken");
const authConfig = require("../config/auth");

function tokenGenerator(params = {}) {
  return jwt.sign({ params }, authConfig.secret, {
    expiresIn: 1500,
  });
}

exports.createUser = async (req, res) => {
  const { email } = req.body;
  try {
    if (await User.findOne({ email })) {
      return res.status(400).send({error: 'User already registered'});
    }
    const user = await User.create(req.body);

    user.password = undefined;

    return res.send({ user, token: tokenGenerator({ id: user.id }) });
  } catch (e) {
    return res.status(400).send({ error: 'Failed' });
  }
};

exports.getAllUsers = async (req, res) => {
  User.find(function (err, user) {
    if (err) return next(err);
    res.send(user);
  });
};

exports.getUserById = async (req, res, next) => {
  User.findById(req.params.id, function (err, user) {
    if (err) return next(err);
    res.send(user);
  });
};

exports.updateUser = async (req, res, next) => {
  User.findByIdAndUpdate(req.params.id, {$set: req.body},function (err, user) {
    if (err) return next(err);
    res.send(user);
  });
};

exports.deleteUser = async (req, res, next) => {
  User.findByIdAndRemove(req.params.id, function (err) {
    if (err) return next(err);
    res.send('Deleted!');
  });
};


