const jwt = require('jsonwebtoken');
const authConfig = require("../config/auth");

module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if(!authHeader) {
        return res.status(401).send({ error: 'Token missed' });
    }

    const [bearer, token] = authHeader.split(' ');

    if(bearer !== "Bearer") {
        return res.status(401).send({ error: 'Token malformed' });
    };

    jwt.verify(token, authConfig.secret, (err, decoded) => {
        if (err) return res.status(401).send({ error: 'Invalid token' });
        req.userId = decoded.id;
        return next();
    });
};
