const express = require('express');
const server = express.Router();

const recipe = require('../controllers/recipes');

server.get('/', recipe.getAllRecipes)
      .post('/', recipe.createRecipe)
      .get('/:id', recipe.getRecipeById)
      .put('/:id', recipe.updateRecipe)
      .delete('/:id', recipe.deleteRecipe)

module.exports = server;