const express = require('express');
const server = express.Router();
const authMiddleware = require("../middleware/auth");
var cors = require('cors')

const user = require('../controllers/users');
const authenticator = require('../controllers/authenticator');

//server.get('/users',authMiddleware, user.details);
server.get('/', cors(), user.getAllUsers)
      .post('/',cors(), user.createUser)
      .get('/:id',cors(), user.getUserById)
      .put('/:id', user.updateUser)
      .delete('/:id', user.deleteUser)
      .post('/authenticate',cors(), authenticator.authenticate);

module.exports = server;