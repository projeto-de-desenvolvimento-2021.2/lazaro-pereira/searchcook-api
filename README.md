# SearchCook-api

## Iniciando localmente a aplicação

- Clone o diretório utilizando `git clone`
- Dentro do diretório principal execute `npm install`
- Instale as dependências `npm install --save express body-parser mongoose`
- Instale o nodemon para atualização em tempo real da aplicação `npm install --save-dev nodemon`
- Inicie a aplicação com o comando `npm run dev`

A aplicação estará rodando localmente na porta 3001

